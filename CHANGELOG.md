# CHANGELOG

## 1.0.0-RC.1

We've now reached 1.0 Release Candidate status!

### BREAKING CHANGES
This first Release Candidate contains several minor breaking changes, which hopefully remove a lot of confusing behavior.

1. **Dropped support for PHP < 7.0**
    Because 7.0 is what we're using in production, we are making use of new 7.0 features.
    
2. **Connection module**
    The connection module has now been melded into the Data module, which makes more sense from a conceptual standpoint. The one major change that this creates is that all apps that get the Pixie query builder from the connection module must now get it from the Data module. For example:

    ```php
    $qb = $this->inject('module.connection')->qb(); // pre 1.0.0-rc.1 behavior

    $qb = $this->inject('module.adapter')->getQueryBuilder(); // >= 1.0.0-rc.1 behavior
    ```

    Also, the configuration property containing database connection parameters has been changed from `connection.config` to simply `database`. This hopefully reflects a better idea of what it is configuring.

    *Note: the above affects only apps using the default implementation of all of the modules.*

    For custom implementations of the Data module, the AdapterInterface has been updated to include two new required methods: `connect()` and `disconnect()`. These essentially map to the old ConnectionInterface `open()` and `close()` methods.

3. **Router layer refactor**
    Because the PHP 7 features allow us to clean up our code a ton, there have been a number of improvements/changes to the router layer that should hopefully make it easier to understand. These changes only affect those using the standard router implementation.

    First, the Utils class available in the standard Routing implementation has been converted to a trait. Instead of calling the static methods on it you should instead `use` it in your class and then call the methods as instance methods.

    Second, the concept of the `$controllers` array in the Router implementation in each app has been removed. It was confusing, and rather unnecessary. However, this does affect calls to `Utils::generateRouteGroup` or `Utils::http`. Previously, `Utils::generateRouteGroup` accepted the controller class name (generally the class in the `$controllers` array) as a second argument. This has instead been moved to the fifth argument, and is null by default. By default, the route group name passed as the first argument to `generateRouteGroup` will be converted into a class name (e.g. 'users' => 'Users'), and the `AppName\Controller` namespace will be prepended to it. If you wish to use a custom controller class in a different namespace than that of `AppName\Controller`, you must pass the full classname as the fifth argument to `generateRouteGroup`.

4. **Container refactor**
    To clean up code, `\FW\Utils\CoreObject` has been removed as a base class, and has instead been replaced by `\FW\Utils\ContainerAwareTrait`. Instead of making your app extend CoreObject (which lead to sometimes odd and unnecesseary class inheritance structures), you can now use it as a trait.

### Remaining Changes

1. **Data layer improvements**
    Internally a lot of things have been cleaned up in the Adapter, and one of these things is that you can call static model methods a little easier than you previously could. (The old way still works, though).

    Pre-1.0RC1 you would call a Model class static method (inside a controller) like this:
    ```
    $result = $this->adapter->call('ModelName', 'findAll', $arg1, $arg2);
    ```

    With 1.0RC1 and later, the new way has been added to accomplish the same behavior more concisely:
    ```
    $result = $this->adapter->findAll('ModelName', $arg1, $arg2);
    ```

2. **For Developers: test script changes**

    Previously, FW Api had a gulpfile that contained the script to handle setup and testing. Now, that has been replaced with raw PHP scripts and commands provided by Composer scripts. This simplifies the development setup completely, dropping any need to have Node installed to work on a PHP only application.

    Biggest change: instead of running `npm test` to run tests locally, you now run `composer run-script test`.

## 1.0.0-beta.22
- [IMPROVEMENT] Pull url from config.json of app if one exists
- gulpfile fix
- add yarn.lock

## 1.0.0-beta.21
- [BREAKING CHANGE] AbstractController loadDependencies method now follows true DI pattern
- [FEATURE] Error handling refactor
- [BUGFIX] Fix model required property parsing
- [BREAKING CHANGE] programID => appID in Loader class

## 1.0.0-beta.20
- [IMPROVEMENT] json view helpers can now be "merged"
- [IMPROVEMENT] Models can now get properties by alias
- [BUGFIX] Custom join calls now still work as intended
- complex Model database calls now use a transaction

## 1.0.0-beta.19
- [FEATURE] Add more array helper methods to composer dependencies

## 1.0.0-beta.18

- **[BREAKING CHANGE]** Config refactor
    - Removed ConfigInterface (BaseConfig is now the config implementation)
    - Config is now no longer a module
- **[BREAKING CHANGE]** App/Addon Load Cycle fixes
    - Addon `#load` method now called after all modules have been initialized
- **[BREAKING CHANGE]** Injection Mechanism Refactor
    - Cleaner injection mechanism via new `CoreObject` class (available from `\FW\Utils\CoreObject`)
    - Remove old InjectObject utility
- documentation updates
- dependency updates

## 1.0.0-beta.17

- [BUGFIX/FEATURE] Add events API to fix addon load cycle issues
- Shippable build fixes
- Documentation updates

## 1.0.0-beta.16

- [BUGFIX] switch Pixie version to back-ported PHP one
- **[BREAKING CHANGE/FEATURE]** - Route Utils now initialized by default in the AbstractRouter setup method
- **[BREAKING CHANGE]** - Remove unnecessary first argument from `Utils::generateRouteGroup()` method
- **[BREAKING CHANGE]** - Privatize error and logging modules
- Documentation updates
