<?php
// @codingStandardsIgnoreStart
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

/**
 * Exception class that formats an HTTP status error code
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
class StatusException extends \Exception {

    /**
     * Default message to output
     * @var string
     */
    protected static $defaultMessage = 'An error occured. Check your logs for more details.';

    /**
     * Default status code to output
     * @var integer
     */
    protected static $defaultCode = 500;

    protected static $type = 'ServerError';

    /**
     * Constructs the exception
     * @param string          $message  message to send
     * @param integer         $code     HTTP status of the exception
     * @param \Exception|null $previous
     */
    public function __construct($message = '', $code = null, $type = '', \Exception $previous = null) {
        if (!$message) {
            $message = static::$defaultMessage;
        }

        if (!$code) {
            $code = static::$defaultCode;
        }

        if ($type && $type instanceof \Exception) {
            // This is here to provide backwards compatability
            $previous = $type;
            $type = null;
        }

        $errorJson = json_encode(array(
            'error' => true,
            'message' => $message,
            'type' => ($type && is_string($type)) ? $type : static::$type
        ));

        parent::__construct($errorJson, $code, $previous);
    }
}
//@codingStandardsIgnoreEnd
