<?php

if (!file_exists('./vendor/autoload.php')) die;

require './vendor/autoload.php';
$fs = new Symfony\Component\Filesystem\Filesystem();
$fs->remove('./vendor');
