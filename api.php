<?php
/**
 * Benner Informatics Framework API
 * @copyright Copyright (c) 2014 Benner Library Informatics (http://bennerlibrary.com)
 * @author Austin Burdine <acburdine@gmail.com>
 *
 * @license MIT
 */

/**
 * API Hook file
 *
 * This is what boots up the FW App System API, and handles all app server requests.
 *
 * It's really the only bit of procedural code in here.
 */
class FW {

    public static function init($appCode, $autoRun = true, array $modules = [], $appDir = './apps') {
        // Step 1: Change the current working directory to
        // FW Api's base directory path
        chdir(__DIR__);

        // Step 2: Define the APPCODE constant.
        // This is used across the system to know which app
        // is currently being run/loaded
        define('APPCODE', $appCode);

        // Step 3: Check if composer has been installed. If not
        // an error should be output to the client because without
        // composer nothing will work correctly
        if (!file_exists('./core/vendor/autoload.php')) {
            http_response_code(501);

            echo json_encode([
                'error' => true,
                'type' => 'RunError',
                'message' => 'Composer dependencies have not been installed,' .
                    'and thus the FW App System cannot boot.<br>Please run <code>composer install</code>' .
                    'on your system.'
            ]);

            die; // Die because we can't really run the application now :(
        }

        // Step 4: Require the Composer Autoloader, which
        // handles all of the fancy class loading
        $classLoader = require('./core/vendor/autoload.php');

        // Step 5: Discover Apps
        $apps = \FW\Utils\System::discoverApps($appDir);

        // Step 6: Check if the app we're trying to run is a valid app
        if (!isset($apps[$appCode])) {
            http_response_code(501);

            echo json_encode([
                'error' => true,
                'type' => 'RunError',
                'message' => "App '{$appCode}' is not installed."
            ]);

            die;
        }

        // Step 7: Instantiate the FW App Loader, which handles all
        // of the app's boot processing
        $appLoader = new FW\Loader($classLoader, $apps);

        // Step 8: Load the app
        if (!$autoRun && $modules) {
            $app = $appLoader->load($appCode, $modules);
        } else {
            $app = $appLoader->load($appCode);
        }

        // Step 9: If the app should be run, run it.
        if ($autoRun) {
            $app->run();
            return;
        }

        // Step 10: Return the app if it needs to be used.
        return $app;
    }
}
