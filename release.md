# Release Notes

## 1.0.0-beta.21
- **BREAKING CHANGE**: AbstractController loadDependencies method
- Fixed some wrong behavior with the model class
- Added new `type` property to error JSON response
    - StatusException (and other exceptions that extend StatusException) can now use type to return more specific information
- **BREAKING CHANGE**: Rename programID to appID to reflect Group Control changes

## 1.0.0-beta.20
- Fixed some odd behavior with the Model class
- Improvements to the Model class
- Json view helper improvements

## 1.0.0-beta.19

- Added polyfill support for array_column function available in php 5.5 (you can now use it in php version > 5.3) - See the [array_column docs](http://php.net/manual/en/function.array-column.php)
- Added a bunch of array utilities (ported from the Laravel framework) - see the [docs](https://github.com/weew/helpers-array-legacy#functions)

## 1.0.0-beta.18

- **BREAKING CHANGE**: Config Refactor
    - Removed the ConfigInterface class, extend `FW\Config\BaseConfig` instead
    - Any references to ConfigInterface should be moved to BaseConfig references
- **BREAKING CHANGE**: Addon load cycle fixes
    - Addon load method is now called _after_ all of the modules have been initialized, so addons can now modify/extend different modules in the `load` method. This fixes some isses where some addons had to use the events API in order to make certain things possible.
- **BREAKING CHANGE**: Injection Refactor
    - Removed the InjectionInterface class
    - Added the CoreObject class
    - Because of this refactor, you no longer have to do things like `Some\Injection\Class::inject($this->container)` in your app's onLoad method. You can now extend the `CoreObject` class and inject any container variables you need to into your class using the `CoreObject::inject()` method.

    Example:


        <?php

        namespace MyApp;

        class SomeClassThatNeedsAContainerProperty extends \FW\Utils\CoreObject {
            public function someMethod() {
                $connection = self::inject('module.connection');
            }
        }
