# Informatics FW App System API

[![](https://img.shields.io/shippable/565071341895ca4474240b17.svg)](https://app.shippable.com/projects/565071341895ca4474240b17)
![](https://img.shields.io/badge/version-1.0.0--RC.9-green.svg)

---

The [FW App System](http://linformatics.bitbucket.org) API is the library containing the necessary components to tie together all of the applications included with the FW App System. It is modular, configurable, and extensible.

# Documentation

- [Guides](http://linformatics.bitbucket.org/docs/main/)
- [API Documentation](http://linformatics.bitbucket.org/api/fw-api/latest/)

# Changelog

- [Changelog](CHANGELOG.md)
- [Release Notes](release.md)

# Developing

To begin developing FW API, use the following steps: 

- Delete the contents of your FW API core folder (If standard fw setup, this would be the C:/User/{username}/webroot/fw-dev/core folder).
- Then clone this repository into that folder (`git clone https://bitbucket.org/linformatics/fw-api`)

To release the fw-api complete the following steps:

- Increment version number in composer.json
- Run `php run-script release` in your core folder
- Upload the resulting zip file in Downloads tab of this repository (should be formatted as fw-api-{version#}

