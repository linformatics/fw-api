<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\View;

use FW\Structures\Module\ModuleInterface;

/**
 * Interface for the view module
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
interface ViewInterface extends ModuleInterface {

    /**
     * Gets the current content type of the view output
     * @return string The content type as a string
     */
    public function getContentType();
}
