<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\View\Standard\Helpers;

use FW\View\Standard\HelperInterface;

/**
 * Json view implementation of the view module
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
class JsonHelper implements HelperInterface {

    /**
     * Array of data to render
     * @var array
     */
    protected $data;

    /**
     * Constructs the view module
     */
    public function __construct() {
        $this->data = array();
    }

    /**
     * Adds stuff to render
     * @param mixed  $object  array or object to add
     * @param string  $key    key to add $object at
     * @param boolean $recur  whether or not $object is a two-dimensional array
     */
    public function add($object, $key = '', $recur = true) {
        if (is_null($object)) {
            return $this;
        }
        $toAdd = $object;
        if (is_array($toAdd) && $recur) {
            foreach ($toAdd as $arKey => $arValue) {
                if (is_null($arValue)) {
                    unset($toAdd[$arKey]);
                    continue;
                }
                if (is_object($arValue)) {
                    $arValue = $this->serializeObject($arValue);
                }
                $toAdd[$arKey] = $arValue;
            }
        } else if (is_object($toAdd)) {
            $toAdd = $this->serializeObject($toAdd);
        }
        if (!$key) {
            if (is_array($toAdd)) {
                $this->data = array_merge($this->data, $toAdd);
            } else {
                $this->data[] = $toAdd;
            }
        } else {
            $this->data[$key] = $toAdd;
        }
        return $this;
    }

    /**
     * Gets a variable that will be rendered
     * @param  string $key variable to get
     * @return mixed       variable gotten
     */
    public function get($key) {
        if ($this->has($key)) {
            return $this->data[$key];
        }
        return null;
    }

    /**
     * Sets a value to the data array
     * @param string $key   key to set
     * @param mixed $value value to set
     * @return JsonHelper $this
     */
    public function set($key, $value) {
        if ($this->has($key)) {
            unset($this->data[$key]);
        }
        $this->add($value, $key);
        return $this;
    }

    /**
     * If the render data has a key
     * @param  string  $key key to check
     * @return boolean      if the data contains the key
     */
    public function has($key) {
        return array_key_exists($key, $this->data);
    }

    /**
     * Clears the data array
     * @return JsonView this object
     */
    public function clear() {
        $this->data = array();
        return $this;
    }

    /**
     * Renders the data and returns it
     * @param  boolean $forceObject whether or not to force json object behavior
     * @return string               rendered json data as string
     */
    public function toString($forceObject = false) {
        if ($forceObject || !((bool) $this->data)) {
            return json_encode($this->data, JSON_FORCE_OBJECT);
        } else {
            return json_encode($this->data);
        }
    }

    /**
     * Renders the data
     * @param  boolean $forceObject whether or not the force json object behavior
     */
    public function render($forceObject = false) {
        echo $this->toString($forceObject);
    }

    /**
     * Returns the content type
     * @return string content-type
     */
    public function getContentType() {
        return 'application/json';
    }

    /**
     * Function used in unit testing to get data
     * @access private
     */
    public function getData() {
        return $this->data;
    }

    /**
     * Serializes an object
     * @param  mixed $object
     * @return array the serialized object
     */
    protected function serializeObject($object) {
        if (is_a($object, __CLASS__)) {
            return $object->getData();
        }

        if (method_exists($object, 'toJson')) {
            return $object->toJson();
        }
        return get_object_vars($object);
    }
}
