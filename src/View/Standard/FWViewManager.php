<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\View\Standard;

use FW\Structures\Module\AbstractModule;

/**
 * View manager class
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
class FWViewManager extends AbstractModule implements \FW\View\ViewInterface {

    /**
     * Array of helper objects that have been registered
     * @var array
     */
    protected $helpers = array();

    /**
     * The content type of the last called helper
     * @var HelperInterface
     */
    protected $currentHelper;

    /**
     * {@inheritdoc}
     */
    public function init() {
        $this->registerHelper('\FW\View\Standard\Helpers\JsonHelper', 'json');
    }

    /**
     * Registers a helper
     *
     * If alias is provided, the helper is accessible via
     * $view->helper('whatever-alias-you-provided'). If not,
     * it will be accessible through the View classname.
     *
     * @param string $className Classname of the helper to register
     * @param string|null $alias Alias of the class if desired
     */
    public function registerHelper($className, $alias = null) {
        if (($alias && !array_key_exists($alias, $this->helpers)) || !array_key_exists($className, $this->helpers)) {
            if (is_a($className, '\FW\View\Standard\HelperInterface', true)) {
                $key = ($alias) ? $alias : $className;
                $this->helpers[$key] = $className;
            }
        }
    }

    /**
     * Retrieves a helper
     *
     * @param string $name The name of the helper
     * @return \FW\View\Standard\HelperInterface|null
     */
    public function helper($name) {
        if (array_key_exists($name, $this->helpers)) {
            $helper = new $this->helpers[$name]();
            $this->currentHelper = $helper;
            return $helper;
        }
        return null;
    }

    /**
     * Gets the content type
     *
     * @return string The content type
     */
    public function getContentType() {
        if ($this->currentHelper) {
            return $this->currentHelper->getContentType();
        }
        return 'text/plain';
    }
}
