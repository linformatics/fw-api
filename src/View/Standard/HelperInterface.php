<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\View\Standard;

/**
 * Interface for View Helpers
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
interface HelperInterface {

    /**
     * Clear all helper data
     */
    public function clear();

    /**
     * Export data as a string
     */
    public function toString();

    /**
     * Output content to frontend
     */
    public function render();

    /**
     * Get the content type of the helper
     */
    public function getContentType();
}
