<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Data\Standard;

use FW\Data;

/**
 * Model abstraction
 *
 * @author Austin Burdine <aburdine@gmail.com>
 * @since  1.0.0
 */
abstract class Model {

    /**
     * Constructs the class. Also converts GROUP_CONCAT fields to arrays
     */
    public function __construct() {
        $includes = func_get_args();

        $links = self::$modelLinks;

        foreach ($includes as $include) {
            if (array_key_exists($include, $links)) {
                if (!isset($this->links) || !$this->links) {
                    $this->links = array();
                }

                $this->links[$include] = $links[$include]($this->get(static::$id));
                unset($this->$include);
            } else {
                if (isset($this->$include)) {
                    if(substr($this->$include, 0, 1) === '{') {
                        $this->$include = json_decode("[{$this->$include}]", true);
                    } else {
                        $this->$include = array_filter(explode(",", $this->$include));
                    }
                }
            }
        }
    }

    /**
     * Gets a value
     * @param  string $key Model property to get
     * @return mixed Value of model property
     */
    public function get($key) {
        if (isset(static::$aliases[$key]) && property_exists($this, static::$aliases[$key])) {
            $aliasedKey = static::$aliases[$key];
            return $this->$aliasedKey;
        } else if (property_exists($this, $key)) {
            return $this->$key;
        } else {
            return null;
        }
    }

    /**
     * Sets a value
     * @param string $key Model property to set
     * @param mixed $value Value to set
     */
    public function set($key, $value) {
        if (property_exists($this, $key)) {
            $this->$key = $value;
        }
        return $this;
    }

    /**
     * Provides an easy way to return the aliased data all at once
     * @return array
     */
    public function toJson() {
        $data = $this->alias(get_object_vars($this), true);
        return $data;
    }

    ////////////////////////////////////
    // END INSTANCE VARIABLES/METHODS //
    // BEGIN STATIC CLASS METHODS     //
    ////////////////////////////////////

    /**
     * QueryBuilder instance
     * @var \Pixie\QueryBuilder\QueryBuilderHandler
     */
    protected static $qb;

    /**
     * Table name (without a prefix if there is one)
     * @var string
     */
    protected static $table;

    /**
     * name of the primary key column
     * @var string
     */
    protected static $id;

    /**
     * Allowed joinable columns
     *
     * These can be one-to-many or many-to-many
     * @var array
     */
    protected static $allowedJoins = array();

    /**
     * Allowed searchable columns
     * @var array
     */
    protected static $searchable = array();

    /**
     * Aliases to convert from client data format
     * to server format
     *
     * Aliases should be an array with the keys
     * representing the client side format
     * and the values representing the server side.
     *
     * E.G. Client property = 'id'
     *      Server property = 'userID'
     *
     *      $aliases = array('id'=>'userID')
     *
     * @var array
     */
    protected static $aliases = array();

    /**
     * Array or required columns (used for inserts and updates)
     * @var array
     */
    protected static $required = array();

    /**
     * Static array of fields
     * Can either be set statically or can be generated
     * by the available instance variables
     *
     * @var array
     */
    protected static $fields = null;

    /**
     * Static array of filters
     * @var array
     */
    protected static $filters = array();

    /**
     * Links to other model data
     *
     * @var array
     */
    protected static $modelLinks = array();

    /**
     * Sets the query builder instance
     * @param \Pixie\QueryBuilder\QueryBuilderHandler $qb
     */
    public static function setQueryBuilder($qb) {
        static::$qb = $qb;
    }

    /**
     * Concise way of joining another table based on id comparison
     * @param  \Pixie\QueryBuilder\QueryBuilderHandler  $qb QueryBuilder instance
     * @param  string  $name     name of generated column of ids
     * @param  string  $table    table name (without prefix if one exists)
     * @param  string  $key      id column name of table to compare
     * @param  boolean $isToMany if the relationship is one-to-many, this will be set to false
     * @param  string  $id       id column name of the current table in the join
     * @param  \Closure $joinOn  if there are extra join rules use them
     */
    protected static function joinMany($qb, $name, $table, $key, $isToMany = true, $joinOn = null, $id = null) {
        $id = $id ?? static::prefixForeignKey(static::$id);
        if(is_array($key)) {
            $key = join(', ', array_map(function($name, $key) use ($table, $qb) {
                if(is_numeric($name)) {
                    $name = $key;
                }
                $key = $qb->addTablePrefix($table.'.'.$key);
                return "\"$name\":\"', COALESCE($key, ''), '\"";
            }, array_keys($key), $key));
            $idKey = $qb->addTablePrefix($table.'.'.$id);
            $key = "IF($idKey IS NOT NULL, CONCAT('{{$key}}'), NULL)";
        } else {
            $key = ($isToMany) ? static::prefixForeignKey($key) : $key;
            $key = $qb->addTablePrefix($table.'.'.$key);
        }
        $qb->select($qb->raw("COALESCE(GROUP_CONCAT(DISTINCT($key) SEPARATOR ','), '') AS `$name`"));
        if ($joinOn instanceof \Closure) {
            $staticId = static::$id;
            $qb->leftJoin($table, function ($tb) use ($staticId, $table, $id, $joinOn) {
                $tb->on($staticId, '=', $table.'.'.$id);
                $joinOn($tb);
            });
        } else {
            $qb->leftJoin($table, static::$id, '=', $table.'.'.$id);
        }
        $qb->groupBy(static::$id);
    }

    /**
     * Handles the insert/update side of the relationship based on id comparison
     *
     * Used in many-to-many relationships
     * @param  \Pixie\QueryBuilder\QueryBuilderHandler $qb     QueryBuilder Instance
     * @param  string $table  Table name (without prefix)
     * @param  string $key    id column name of $table
     * @param  string|int $id     ID of current row
     * @param  array $values Array of values to be placed in the join table
     */
    protected static function insertMany($qb, $table, $key, $id, $values) {
        if (!is_array($values)) {
            return;
        }
        $keys = static::prefixForeignKey(array(static::$id, $key));

        $qb->transaction(function ($qb) use ($keys, $id, $values, $table) {
            $qb->table($table)->where($keys[0], $id)->delete();

            $toInsert = array();
            foreach ($values as $value) {
                $toInsert[] = array($keys[0] => $id, $keys[1] => $value);
            }

            if ($toInsert) {
                $qb->table($table)->insert($toInsert);
            }
        });
    }

    /**
     * Prefixes $values with a predefined foreign key prefix
     * @param  string|array $values String or array of strings to prefix
     * @param  string $prefix string to prefix with
     * @return string|array         String or array of prefixed values
     */
    protected static function prefixForeignKey($values, $prefix = 'FK_') {
        if (is_array($values)) {
            return array_map(function ($value) use ($prefix) {
                return $prefix.$value;
            }, $values);
        } else {
            return $prefix.$values;
        }
    }

    /**
     * Initiates the model. Sets the "fields" variable if it exists
     * @param  array  $initialValues optional array of inital values to add to the query
     * @return \Pixie\QueryBuilder\QueryBuilderHandler                QueryBuilder instance associated with the object
     */
    public static function forge(array $initialValues = array()) {
        $qb = static::$qb->table(static::$table);
        foreach ($initialValues as $key => $value) {
            if ($key == 'id') {
                $key = static::$id;
            }
            $qb->where($key, $value);
        }
        static::initFields();
        return $qb;
    }

    /**
     * Find one model record
     * @param  array  $searchValues Values to search by (associative array)
     *                              Prefix the key with ! for not equals check
     *                              NOTE: if one of the values in the key-value
     *                              pair is an array, it will be treated as an 'IN'
     *                              check instead of an '=' check
     * @param  array  $includes     Joinable relationships to include
     * @param  array  $fields       List of fields to return
     * @param  boolean $runFilters  If false, skip running filters. Used internally by add, edit, and editMany to prevent the returns from being caught
     *                              For example, if we edit based on a filter, the returns may no longer match the filter
     * @return Model               Values from the database as a Model object
     */
    public static function findOne(array $searchValues, $includes = array(), array $fields = array(), $runFilters = true) {
        $qb = static::forge();
        static::findProcess($qb, $searchValues, $includes, $fields);
        if ($runFilters) {
            static::runFilters($qb);
        }
        return $qb->asObject(get_called_class(), $includes)->first();
    }

    /**
     * Find all the model records
     * @param  array  $searchValues Values to search by (associative array)
     *                              Prefix the key with ! for not equals check
     *                              NOTE: if one of the values in the key-value
     *                              pair is an array, it will be treated as an 'IN'
     *                              check instead of an '=' check
     * @param  array  $includes     Joinable relationships to include
     * @param  array  $fields       List of fields to return
     * @param  boolean $runFilters  If false, skip running filters. Used internally by add, edit, and editMany to prevent the returns from being caught
     *                              For example, if we edit based on a filter, the returns may no longer match the filter
     * @return array               Array of model classes
     */
    public static function findAll(array $searchValues, $includes = array(), array $fields = array(), $runFilters = true) {
        $qb = static::forge();
        static::findProcess($qb, $searchValues, $includes, $fields);
        if ($runFilters) {
            static::runFilters($qb);
        }
        return $qb->asObject(get_called_class(), $includes)->get();
    }

    /**
     * Gets a count of record models for the specified search. Useful for pagination
     * @param  array  $searchValues Values to search by (associative array)
     *                              Prefix the key with ! for not equals check
     *                              NOTE: if one of the values in the key-value
     *                              pair is an array, it will be treated as an 'IN'
     *                              check instead of an '=' check
     * @return integer              Number of results
     */
    public static function count(array $searchValues) {
        $qb = static::forge();
        static::searchProcess($qb, $searchValues);
        static::runFilters($qb);
        return $qb->count();
    }

    /**
     * Helper used by findOne and findAll to handle some of the join and search legwork
     * @param  \Pixie\QueryBuilder\QueryBuilderHandler $qb           QueryBuilder instance
     * @param  array  $searchValues Values to search by
     * @param  array  $includes     Relationships to include
     * @param  array  $fields       Array of fields to return
     */
    protected static function findProcess($qb, array $searchValues, &$includes, array $fields) {
        $qb->select($fields ?: static::$fields);

        static::searchProcess($qb, $searchValues);

        if (!is_array($includes)) {
            $includes = array($includes);
        }

        foreach ($includes as $include) {
            if (in_array($include, static::$allowedJoins)) {
                static::$include('select', $qb);
            }
        }
    }

    /**
     * Helper used by findProcess, deleteMany, and editMany to process search keys
     * @param  \Pixie\QueryBuilder\QueryBuilderHandler $qb          QueryBuilder instance
     * @param  array  $searchValues Values to search by
     */
    protected static function searchProcess(&$qb, array $searchValues) {
        $searchValues = static::alias($searchValues);
        foreach ($searchValues as $key => $value) {
            // if the first character of a key is !, invert the search
            if ($key[0] === '!') {
                $search = 'whereNot';
                $key = substr($key, 1); // trim the ! off the key
            } else {
                $search = 'where';
            }
            if (in_array($key, static::$searchable)) {
                // determine if we want whereIn, whereNull, or a standard where by type
                if (is_null($value)) {
                    $qb->{$search . 'Null'}($key);
                } else {
                    $qb->{$search . (is_array($value) ? 'In' : '')}($key, $value);
                }
            }
        }
    }

    /**
     * Convenience method used to find a singe record via the record's primary key
     * @param mixed $id Value in the primary key column to check
     * @param array $includes Relationships to include
     * @return mixed|null model record, or null if not found
     */
    public static function findById($id, $includes = array(), array $fields = array()) {
        return static::findOne(array(static::$id => $id), $includes, $fields);
    }

    /**
     * Adds a new record to the database
     * @param array $values   Values to add
     * @param array $includes Includes to include
     * @return Model The newly added model class
     */
    public static function add(array $values, array $includes = array()) {
        $qb = static::forge();
        $providedId = isset($values['id']) ? $values['id'] : null;
        $values = static::alias($values);

        $filteredValues = array_intersect_key($values, array_flip(static::$fields));
        $joins = array_intersect_key($values, array_flip(static::$allowedJoins));

        $missingFields = static::hasRequired($filteredValues);

        if (count($missingFields)) {
            $missingFieldsString = implode(', ', $missingFields);
            throw new Exception\InvalidDataException("Missing fields with request: {$missingFieldsString}");
        }

        static::runFilters($qb);
        $id = $qb->insert($filteredValues);

        if ($providedId) {
            $id = $providedId;
        }

        foreach ($joins as $join => $value) {
            static::$join('insert', $qb, $id, $value);
        }

        return static::findOne(array('id' => $id), $includes, [], false);
    }

    /**
     * Builds the array for onDuplicateKeyUpdate Used internally in updateMany
     * @param  array $fields Fields for keys to update. Does not support aliases
     * @return array  Associative array used for Pixie to make the request
     */
    protected static function buildOnDuplicateData(array $fields = null) {
        $qb = static::forge();
        $fields = $fields ?: static::$fields;

        $data = [];
        foreach($fields as $field) {
            $data[$field] = $qb->raw("VALUES(`$field`)");
        }
        return $data;
    }

    /**
     * Saves many models in one large database request. Handles editing or adding automatically.
     * Note that if any model edits a key, all edited models with that key set will set it to null due to how insert is handled internally
     * @param  array $valuesArray Array of model JSON. Same format as edit or add, but one per model changing
     * @param  array $includes    Any relationships to include in the output. Note that relationships cannot be saved using this
     * @return array              Array of models that were added or edited
     */
    public static function updateMany(array $valuesArray, array $includes = []) {
        // jump out now if empty
        if (empty($valuesArray)) {
            return [];
        }
        $qb = static::forge();

        // alias keys and remove invalid values
        $validKeys = array_flip(static::$fields);
        $valuesArray = array_map(function($values) use ($validKeys) {
            return array_intersect_key(static::alias($values), $validKeys);
        }, $valuesArray);

        // find a list of all used keys for update handling
        $idKey = static::$id;
        $usedKeys = array_keys(array_reduce($valuesArray, function($keys, $values) use ($idKey) {
            // only get keys of phpcredits
            if (isset($keys[$idKey])) {
                foreach (array_keys($values) as $key) {
                    $keys[$key] = true;
                }
            }
            return $keys;
        }, []));

        // sort values into insert and update
        $ids = [];
        $inserts = [];
        $updates = [];
        foreach ($valuesArray as $values) {
            // if editing, store the ID for the return later
            if (isset($values[static::$id])) {
                $ids[] = $values[static::$id];

                // also make sure any values that get updated are set
                foreach ($usedKeys as $key) {
                    if (!isset($values[$key])) {
                        $values[$key] = null;
                    }
                }
                $updates[] = $values;
            } else {
                $inserts[] = $values;
            }
        }

        // runs the mass insert
        if (!empty($inserts)) {
            $ids = array_merge($qb->table(static::$table)->insert($inserts), $ids);
        }
        // mass update
        if (!empty($updates)) {
            // unfortunately, no good way to do this, so just do an insert on duplicate
            // side effect is any unset keys will nul if set in another entry, but for our uses that is okay
            // if that becomes a problem in the future, consider generating raw SQL and adding it in one raw query. Just make sure to sanitize the inputs
            $qb->onDuplicateKeyUpdate(static::buildOnDuplicateData($usedKeys))->insert($updates);
        }

        // return edited values
        return static::findAll(['id' => $ids], $includes, [], false);
    }

    /**
     * Edits an existing database record
     * @param  string|int $id       Primary Key of model record
     * @param  array  $values   Values to update
     * @param  array  $includes Joins to include in select
     * @return Model           Updated Model object
     */
    public static function edit($id, array $values, array $includes = array()) {
        $qb = static::forge();
        $values = static::alias($values);

        $filteredValues = array_intersect_key($values, array_flip(static::$fields));
        $joins = array_intersect_key($values, array_flip(static::$allowedJoins));

        static::runFilters($qb);
        if (!empty($filteredValues)) {
            $qb->where(static::$id, $id)->update($filteredValues);
        }

        foreach ($joins as $join => $value) {
            static::$join('update', $qb, $id, $value);
        }

        return static::findOne(array('id' => $id), $includes, [], false);
    }

    /**
     * Find all the model records
     * @param  array  $searchValues Values to search by (associative array)
     *                              Prefix the key with ! for not equals check
     *                              NOTE: if one of the values in the key-value
     *                              pair is an array, it will be treated as an 'IN'
     *                              check instead of an '=' check
     * @param  array  $values   Values to update
     * @param  array  $includes     Joinable relationships to include
     * @return array               Array of model classes
     */
    public static function editMany(array $searchValues, array $values, $includes = array(), array $fields = array()) {
        // first, get a list of IDs we will edit for the sake of joins and returns
        // we only care about the ID so no includes are passed along
        $ids = static::findAll($searchValues, [], [static::$id]);
        $ids = array_map(function ($model) {
            return $model->get(static::$id);
        }, $ids);

        // nothing to edit, just leave right here as we get errors otherwise
        if (empty($ids)) {
            return;
        }

        // now, start the editing
        $qb = static::forge();
        $values = static::alias($values);

        $filteredValues = array_intersect_key($values, array_flip(static::$fields));
        $joins = array_intersect_key($values, array_flip(static::$allowedJoins));

        static::searchProcess($qb, $searchValues);
        static::runFilters($qb);
        $qb->update($filteredValues);

        foreach ($joins as $join => $value) {
            foreach ($ids as $id) {
                static::$join('update', $qb, $id, $value);
            }
        }

        return static::findAll(['id' => $ids], $includes, [], false);
    }

    /**
     * Deletes an existing model record
     * @param  string|int $id Primary Key value
     */
    public static function delete($id) {
        $qb = static::$qb;
        $qb->table(static::$table)->where(static::$id, $id)->delete();
    }

    /**
     * Deletes multiple model records based on a search filter
     * @param  array  $searchValues Values to search by (associative array)
     *                              Prefix the key with ! for not equals check
     *                              NOTE: if one of the values in the key-value
     *                              pair is an array, it will be treated as an 'IN'
     *                              check instead of an '=' check
     */
    public static function deleteMany(array $searchValues, $includes = array(), array $fields = array()) {
        $qb = static::$qb->table(static::$table);
        static::searchProcess($qb, $searchValues);
        static::runFilters($qb);
        $qb->delete();
    }

    /**
     * Adds a filter to the list of filters
     *
     * @param \Closure $fn Filter callback
     */
    public static function addFilter(\Closure $fn) {
        static::$filters[] = $fn;
    }

    /**
     * Clears all applied filters
     */
    public static function clearFilters() {
        static::$filters = [];
    }

    /**
     * Adds a link to the list of model links
     * @param string $name name of the property
     * @param \Closure $fn Function to call for the link
     */
    protected static function addLink($name, $fn) {
        static::$modelLinks[$name] = $fn;
    }

    /**
     * Filters things
     *
     * @param \Pixie\QueryBuilder\QueryBuilderHandler $qb QueryBuilder instance
     */
    protected static function runFilters($qb) {
        foreach (static::$filters as $filter) {
            $filter($qb);
        }
    }

    /**
     * Applies the values of self::$alias to an array
     * @param  array  $data Array to alias
     * @param  boolean $flip Whether or not to flip the alias (used for sending data back to the client)
     * @return array        The aliased array
     */
    public static function alias($data, $flip = false) {
        $new = array();
        $aliases = ($flip) ? array_flip(static::$aliases) : static::$aliases;

        foreach ($data as $key => $value) {
            // temporarily cut off modifier
            $mod = '';
            if ($key[0] === '!') {
                $mod = '!';
                $key = substr($key, 1);
            }
            if (array_key_exists($key, $aliases)) {
                $key = $aliases[$key];
            }
            // restore modifier
            $key = $mod . $key;
            $new[$key] = $value;
        }
        return $new;
    }

    /**
     * Ensures that the passed in values supply all the required values
     *
     * @param array $values Values to check
     * @return boolean  Whether or not $values contains all needed properties
     */
    protected static function hasRequired($values) {
        $required = (array_is_indexed(static::$required)) ? array_flip(static::$required) : static::$required;

        return array_keys(array_diff_key($required, $values));
    }

    /**
     * Initialize the fields (pull them from class properties)
     */
    protected static function initFields() {
        if (is_null(static::$fields)) {
            static::$fields = array();
            foreach (get_class_vars(get_called_class()) as $key => $value) {
                if (!isset(static::$$key) && !in_array($key, static::$allowedJoins)) {
                    static::$fields[] = $key;
                }
            }
        }
    }
}
