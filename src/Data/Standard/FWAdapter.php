<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Data\Standard;

use FW\Config;
use FW\Structures\Module;
use FW\Data\AdapterInterface;

use Stringy\StaticStringy as S;
use Pixie\Connection;
use Pixie\QueryBuilder\QueryBuilderHandler as QueryBuilder;

/**
 * Adapter module implementation
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  0.1.0
 */
class FWAdapter extends Module\AbstractModule implements AdapterInterface {

    /**
     * Config module
     * @var \FW\Config\BaseConfig
     */
    protected $config;

    /**
     * Connection module
     * @var \Pixie\Connection
     */
    protected $connection;

    /**
     * Whether or not a connection has been established
     * @var boolean
     */
    protected $isConnected;

    public function connect() {
        if ($this->isConnected) {
            return;
        }

        if (!$this->config->has('database')) {
            throw new \StatusException('Missing database configuration.', 500, 'AdapterError');
        }

        $params = $this->config->get('database');

        // don't throw the PDO exception as it contains parts of the database credentials
        // and this exception is visible to the client
        try {
            $this->connection = new Connection($params['driver'], $params);
        } catch (\PDOException $e) {
            // rethrow exception if the debug option is enabled
            if ($this->config->get('debug-database-errors', false)) {
                throw $e;
            }
            throw new \StatusException('Failed to connect to the database.', 500, 'AdapterError');
        }

        $this->isConnected = true;

        // The database could potentially be in a different timezone then the server,
        // so we need to sync them together
        $tzOffset = date('P'); // something like +05:00 or -06:00
        $qb = $this->getQueryBuilder();
        $qb->query("SET time_zone='{$tzOffset}'");

        // set sql variables as requested
        if ($this->config->has('sql-options')) {
            foreach ($this->config->get('sql-options') as $key => $value) {
                // quote strings
                if (!is_numeric($value)) {
                    $value = $qb->pdo()->quote($value);
                }
                // replace dashes with spaces
                $key = str_replace('-', '_', $key);
                $qb->query("SET $key=$value");
            }
        }
    }

    public function disconnect() {
        $this->connection = null;
    }

    public function __call($name, $args) {
        $model = $this->model(array_shift($args));
        return call_user_func_array([$model, $name], $args);
    }

    public function getPdo(): \PDO {
        $this->connect();
        return $this->connection->getPdoInstance();
    }

    public function getDbName() {
        $this->connect();
        $params = $this->config->get('database');
        return $params['database'];
    }

    public function getQueryBuilder(): QueryBuilder {
        $this->connect();
        return new QueryBuilder($this->connection);
    }

    /**
     * Gets the model name of the class if it exists
     * @param  string $name Shortened name of model (corresponds to app.config)
     * @return string       The fully qualified model name if it exists
     * @throws \FW\Data\Standard\Exception\NoSuchModelException If the model does not exist
     */
    public function model($name) {
        $name = S::upperCamelize($name);
        $modelClass = $this->container->get('namespace') . "Model\\{$name}";

        if (!class_exists($modelClass) || !is_a($modelClass, '\FW\Data\Standard\Model', true)) {
            throw new Exception\NoSuchModelException();
        }

        $this->connect();
        $modelClass::setQueryBuilder($this->getQueryBuilder());
        return $modelClass;
    }

    /**
     * Calls a static function on a specified model
     *
     * @param string $model The model name
     * @param string $method The static method to call
     * @param mixed $args Arguments to pass to the method
     */
    public function call() {
        $args = func_get_args();
        $model = $this->model($args[0]);
        $method = $args[1];
        $args = array_slice($args, 2);

        return call_user_func_array(array($model, $method), $args);
    }

    /**
     * {@inheritdoc}
     */
    public function init() {
        $this->config = $this->container->get('module.config');
    }
}
