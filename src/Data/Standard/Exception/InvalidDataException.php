<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Data\Standard\Exception;

/**
 * Exception class that formats invalid data messages as JSON
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
class InvalidDataException extends \StatusException {

    protected static $type = 'ValidationError';
    protected static $defaultCode = 422;
}
