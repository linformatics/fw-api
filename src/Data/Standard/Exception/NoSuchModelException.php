<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Data\Standard\Exception;

/**
 * Error thrown when a model doesn't exist
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
class NoSuchModelException extends \StatusException {

    /**
     * Default message (used by StatusException)
     * @var string
     */
    protected static $defaultMessage = 'No model with the given name exists.';
}
