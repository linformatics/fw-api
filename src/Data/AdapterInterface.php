<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Data;

use FW\Structures\Module\ModuleInterface;

/**
 * Interface for building data adapter structures
 *
 * All data is handled through at least one AdapterInterface implementation
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
interface AdapterInterface extends ModuleInterface {

    /**
     * Opens a connection to the DB
     */
    public function connect();

    /**
     * Closes a connection
     */
    public function disconnect();
}
