<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Structures\Module;

/**
 * Abstract module class. Contains helpful injection methods
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since 1.0.0
 */
abstract class AbstractModule {

    /**
     * @var \FW\Utils\Container
     */
    protected $container;

    /**
     * Classes that inherit from this class cannot override the constructor.
     * Sets up the injector
     *
     * @param \FW\Utils\Container $c The dependency injection container
     */
    final public function __construct(\FW\Utils\Container $c) {
        $this->container = $c;
    }
}
