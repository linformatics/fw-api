<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Structures\Module;

/**
 * Interface that modules must implement
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since 1.0.0
 */
interface ModuleInterface {

    /**
     * Init method for the module
     */
    public function init();
}
