<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Structures;

/**
 * Abstract main class that all FW-kit apps extend
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since 1.0.0
 */
abstract class App {

    /**
     * Current application instance
     * @var App
     */
    protected static $currentInstance;

    /**
     * App dependency container
     * @var \FW\Utils\Container
     */
    protected $container;

    protected $meta;

    /**
     * Filepath of the application (relative to the fw_api root dir)
     * @var string
     */
    protected $path;

    /**
     * Config Module
     * @var \FW\Config\BaseConfig
     */
    protected $config;

    /**
     * Constructs the app with the given container
     * @param \FW\Utils\Container $container
     */
    public function __construct(\FW\Utils\Container $container) {
        $this->container = $container;
        $this->meta = $container->get('meta');
        $this->path = $this->meta['path'];
        $this->config = $container->get('module.config');

        static::$currentInstance = $this;
    }

    /**
     * Initializes the application
     *
     * This handles all of the module setup and initialization
     * @param array|null $modulesToLoad Optional array of specific modules to load
     * @param boolean $isFull If this app is being booted as a full application or just as a dependent app
     */
    public function init(array $modules = [], bool $isFull = true) {
        if (empty($modules)) {
            $modules = self::$modules;
        }

        if (!$isFull && ($key = array_search('router', $modules)) !== false) {
            unset($modules[$key]);
        }

        foreach ($modules as $module) {
            $this->loadModule($module);
        }

        foreach ($modules as $module) {
            $this->container->get("module.{$module}")->init();
        }

        if ($isFull) {
            $this->onLoad();
        }
    }

    /**
     * Sets up the environment (error handling, debug, etc.)
     *
     * @param string $env The environment to run under (default is development)
     */
    public function setupEnvironment($env = 'development') {
        if ($env = 'dev' || $env == 'development') {
            ini_set('display_errors', 1);
        } else {
            ini_set('display_errors', 0);
        }

        $this->config->set('debug', !($env == 'prod' || $env == 'production'));
        if ($this->config->has('timezone') && $this->config->get('timezone') !== date_default_timezone_get()) {
            date_default_timezone_set($this->config->get('timezone'));
        }
    }

    /**
     * Method to be implemented by app main classes
     * Called at the end of the initialization cycle
     */
    abstract public function onLoad();

    /**
     * If the application is loaded as a dependent app,
     * this method will be called with the parent container instance.
     *
     * @param  \FW\Utils\Container $parentContainer
     */
    public function loadAsDependent($parentContainer) {
    }

    /**
     * Runs the router (called at the end of everything)
     */
    public function run() {
        $this->container->get('module.router')->run();
    }

    /**
     * Loads a module given a name
     *
     * @param string $name The module name
     */
    public function loadModule($name) {
        $moduleClass = $this->config->get('module.' . $name);
        if (array_key_exists($name, self::$interfaces) &&
            !is_a($moduleClass, self::$interfaces[$name], true)) {
            die("No $name module found.");
        }
        $this->container->set('module.' . $name, function ($c) use ($moduleClass) {
            return new $moduleClass($c);
        });
    }

    /**
     * Returns a given value from the container
     * @param  string $key Key to return
     * @return mixed       Value from the container
     */
    public function container($key) {
        return $this->container->get($key);
    }

    /**
     * Get the current application instance
     * @return App the current instance
     */
    public static function getInstance() {
        return static::$currentInstance;
    }

    /**
     * Default module values for the config file
     * @var array
     */
    private static $modules = array(
        'router',
        'adapter',
        'view'
    );

    /**
     * Interfaces for the default set of modules that must
     * be implemented if a custom implementation is created
     *
     * @var array
     */
    private static $interfaces = array(
        'router'        => '\FW\Router\RouterInterface',
        'adapter'       => '\FW\Data\AdapterInterface',
        'view'          => '\FW\View\ViewInterface'
    );
}
