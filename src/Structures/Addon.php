<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Structures;

/**
 * Abstract Addon structure class
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since 1.0.0
 */
abstract class Addon {

    /**
     * Must be implemented by the addon. Sets up anything that needs to
     * be set up
     *
     * @param  \FW\Utils\Container $container Dependency container
     */
    abstract public function load(\FW\Utils\Container $container);
}
