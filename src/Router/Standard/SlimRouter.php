<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Router\Standard;

use FW\Router\RouterInterface;
use FW\Structures\Module\AbstractModule;
use FW\Config;

/**
 * Router Module Implementation
 *
 * Uses the Slim framework (http://slimframework.com)
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
class SlimRouter extends AbstractModule implements RouterInterface {

    /**
     * Application config module
     * @var \FW\Config\BaseConfig
     */
    protected $config;

    /**
     * Adapter module
     * @var \FW\Data\AdapterInterface
     */
    protected $adapter;

    /**
     * View module
     * @var \FW\View\ViewModule
     */
    protected $view;

    /**
     * Slim application instance
     * @var \Slim\Slim
     */
    protected $router;

    /**
     * {@inheritdoc}
     * Initializes variables, mappes routes, etc.
     */
    public function init() {
        $this->config = $this->container->get('module.config');
        $this->adapter = $this->container->get('module.adapter');
        $this->view = $this->container->get('module.view');

        $this->container->set('fw.router', function ($c) {
            $router = new \Slim\Slim();

            $router->config('debug', false);
            $router->error(function (\Exception $e) use ($c) {
                $c->get('error_handler')->handleException($e);
            });

            return $router;
        });

        $this->router = $this->container->get('fw.router');

        $this->loadRouteMapper($this->container->get('namespace') . 'Router');
        $this->router->add(new ContentTypeMiddleware($this->view));
    }

    /**
     * Load the route mapper
     *
     * @param  string $class Route mapper to use
     */
    protected function loadRouteMapper($class) {
        if (!class_exists($class) || !is_a($class, '\FW\Router\Standard\AbstractRouteMapper', true)) {
            return;
        }
        $routeMap = new $class($this->config, $this->router, $this->adapter, $this->view);
        $routeMap->setup(); // optional implemented method to set up any dependencies
        $routeMap->mapRoutes(); // map the routes
        $this->router->notFound([$routeMap, 'handle404']);
        $this->container->set('fw.router.map', $routeMap);
    }

    /**
     * Runs the application
     */
    public function run() {
        $this->router->run();
    }
}
