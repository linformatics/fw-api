<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Router\Standard;

use FW\Utils\Container;

/**
 * Controller abstraction
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
abstract class AbstractController {
    use \FW\Utils\ContainerAwareTrait;

    /**
     * Request object
     * @var \Slim\Http\Request
     */
    protected $request;

    /**
     * Response object
     * @var \Slim\Http\Response
     */
    protected $response;

    /**
     * Route Mapper
     * @var AbstractRouteMapper
     */
    protected $routeMapper;

    /**
     * Adapter module
     * @var \FW\Data\AdapterInterface
     */
    protected $adapter;

    /**
     * View Handler
     * @var \FW\View\ViewInterface
     */
    protected $view;

    /**
     * Config Instance
     * @var \FW\Config\BaseConfig
     */
    protected $config;

    public function checkObject($object, $docName) {
        $value = array_get($object, $docName, null);

        if (is_null($value)) {
            throw new \StatusException("No root key ($docName) provided.", 400, 'BadRequestError');
        }

        return $value;
    }

    /**
     * Setup dependencies using injection
     */
    final public function load(AbstractRouteMapper $map, \Slim\Http\Request $req, \Slim\Http\Response $res) {
        $this->routeMapper = $map;

        $this->request = $req;
        $this->response = $res;
        $this->adapter = $this->inject('module.adapter');
        $this->view = $this->inject('module.view');
        $this->config = $this->inject('module.config');
        $this->afterLoad();
    }

    /**
     * Function called after the controller loads. Designed to be overridden if you need extra loading
     */
    protected function afterLoad() {}

    /**
     * Load an external controller
     * @param  string $className     Classname of the controller to load
     * @return \FW\Router\Standard\AbstractController new controller instance
     */
    final protected function loadController($className) {
        return $this->routeMapper->load($className);
    }
}
