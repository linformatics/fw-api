<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Router\Standard;

use Slim\Slim;
use Stringy\StaticStringy as S;

/**
 * Utiltity functions for applications that use
 * the standard routing implementation
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since 1.0.0
 */
trait RoutingUtils {

    /**
     * Default bread route maps
     * @var array
     */
    private static $breadDefaults = [
        ['/', 'browse', 'GET'],
        ['/:id/', 'read', 'GET'],
        ['/:id/', 'edit', 'PUT'],
        ['/', 'add', 'POST'],
        ['/:id/', 'delete', 'DELETE']
    ];

    /**
     * Generate a group of routes
     *
     * @param  string $name       Name of the group
     * @param  string $controller Controller class name
     * @param  array  $routes     optional array of routes to extend
     * @param  boolean $addTo     if this is true, augment the default list of routes
     *                            instead of replacing them
     */
    public function generateRouteGroup(
        string $name, array $routes = [], bool $addTo = false, string $controller = ''
    ) {
        $routes = !empty($routes) ?
            (($addTo) ? array_merge($routes, self::$breadDefaults): $routes) : self::$breadDefaults;

        // Use controller if provided, otherwise use the uppercamelized route name
        $controller = $controller ?: S::upperCamelize($name);

        $this->router->group('/' . $name, (function () use ($controller, $routes) {
            foreach ($routes as $route) {
                $this->router->map($route[0], $this->http($controller, $route[1]))->via($route[2]);
            }
        })->bindTo($this));
    }

    /**
     * slim route callback wrapper function. Used to abstract out the arguments
     * that slim provides to the function as well as any GET or POST data
     * @param  string  $controller controller classname
     * @param  string  $action     name of the method to call on the controller
     * @param  boolean $isAjax     whether or not calls to this method should be
     *                             limited to only Ajax (XMLHttpRequest) calls
     */
    public function http($controller, $action) {
        return function () use ($controller, $action) {
            $controller = $this->load($controller);

            if (!is_callable(array($controller, $action)) || !method_exists($controller, $action)) {
                throw new \InvalidArgumentException("Controller or action is not valid.");
            }

            // No, the "router->router" is not a typo. $this->router is the Slim instance, and there is
            // a variable "router" on the slim instance as well. That's what we need to access, and
            // the similarity of naming is just something that can't be avoided
            $options = (object) $this->router->router->getCurrentRoute()->getParams();

            $allowedOptions = array_unique(array_merge([
                'includes'
            ], property_exists($controller, 'allowedOptions') ? $controller::$allowedOptions : []));

            $query = $this->router->request->get();

            foreach ($allowedOptions as $option) {
                if (isset($query[$option])) {
                    $value = $query[$option];
                    // if its a string, parse commas into an array
                    if (gettype($value) === 'string') {
                        $value = explode(',', $value);
                        $options->$option = (count($value) > 1) ? $value : $value[0];
                    } else {
                        // otherwise keep the type
                        $options->$option = $value;
                    }
                    unset($query[$option]);
                } else if (property_exists($controller, $option)) {
                    $options->$option = $controller::$$option;
                }
            }

            $options->query = $query;

            $args = [$options];

            if ($this->router->request->isPost() || $this->router->request->isPut()) {
                $object = $this->router->request->post() ?: json_decode($this->router->request->getBody(), true);
                array_unshift($args, $object);
            }

            $response = call_user_func_array([$controller, $action], $args);

            if ($this->router->request->isPost()) {
                $this->router->response->setStatus(201);
            }

            if (is_a($response, '\FW\View\Standard\HelperInterface')) {
                $this->router->response->write($response->toString());
                return;
            }

            $this->router->response->write(json_encode($response ?: [], JSON_FORCE_OBJECT));
        };
    }
}
