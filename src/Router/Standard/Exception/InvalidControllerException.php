<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Router\Standard\Exception;

/**
 * Invalid Controller Exception
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since 1.0.0
 */
class InvalidControllerException extends \Exception {

    /**
     * Construct the error
     */
    public function __construct() {
        parent::__construct('Controller class does not extend \FW\Router\Standard\AbstractController.
            Please see the FW documentation for more details.', 500, null);
    }
}
