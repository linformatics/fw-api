<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Router\Standard;

use Stringy\StaticStringy as S;

/**
 * Abstraction for route mapping classes
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
abstract class AbstractRouteMapper {
    use \FW\Utils\ContainerAwareTrait;

    /**
     * Slim Router
     * @var \Slim\Slim
     */
    protected $router;

    public function __construct() {
        $this->router = $this->inject('fw.router');
    }

    /**
     * Loads a specific controller class
     *
     *
     * @param  string $cClass Full classname of controller class
     * @return \FW\Router\Standard\AbstractController Instance of the controller class
     */
    public function load($cClass) {
        // If a full classname is provided, use it
        if (!class_exists($cClass)) {
            $cClass = $this->inject('namespace') . 'Controller\\' . S::upperCamelize($cClass);

            // check again and make sure it exists
            if (!class_exists($cClass)) {
                throw new Exception\InvalidControllerException();
            }
        }

        $controller = new $cClass();

        if (!is_a($controller, '\FW\Router\Standard\AbstractController')) {
            throw new Exception\InvalidControllerException();
        }

        $controller->load($this, $this->router->request, $this->router->response);
        return $controller;
    }

    /**
     * Maps the routes
     */
    abstract public function mapRoutes();

    /**
     * Optionally implemented setup method
     */
    public function setup() {
    }

    /**
     * Handle a 404 error
     *
     * @throws \StatusException
     */
    public function handle404() {
        $routePath = $this->router->request->getResourceUri();
        throw new \StatusException("Route not found: {$routePath}", 404, 'NotFoundError');
    }
}
