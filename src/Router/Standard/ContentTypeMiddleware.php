<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Router\Standard;

/**
 * Middleware to set the content-type of an application
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
class ContentTypeMiddleware extends \Slim\Middleware {

    /**
     * Content type to set
     * @var string
     */
    protected $view;

    /**
     * Constructs the middleware with the given content type
     * @param string $contentType content-type
     */
    public function __construct(\FW\View\ViewInterface $view) {
        $this->view = $view;
    }

    /**
     * Slim middleware processor
     *
     * Sets the HTTP 'Content-Type' header
     */
    public function call() {
        $this->next->call();

        $this->app->response->headers->set('Content-Type', $this->view->getContentType());
    }
}
