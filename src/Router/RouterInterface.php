<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Router;

use FW\Structures\Module\ModuleInterface;

/**
 * Interface for building Router structures
 *
 * All routes are handled by one (or more) implementation(s) of RouterInterface
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
interface RouterInterface extends ModuleInterface {

    /**
     * Executes the route (tells the router to execute
     * the function(s) associated with a given endpoint)
     *
     * This should be run LAST, after all application stuff has been set up.
     */
    public function run();
}
