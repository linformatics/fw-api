<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW;

use Composer\Autoload\ClassLoader;
use Stringy\StaticStringy as S;

use FW\Utils\System;

/**
 * App Loading class
 *
 * @since 1.0.0
 * @author Austin Burdine <aburdine@olivet.edu>
 */
class Loader {

    /**
     * Default config implementations array
     *
     * @var array
     */
    private static $default_configs = array(
        'php'   => '\FW\Config\PhpConfig',
        'json'  => '\FW\Config\JsonConfig'
    );

    /**
     * This exists to protect against circular dependencies
     * @var array
     */
    private static $loadedApps = array();

    /**
     * Composer loader instance
     * @var \Composer\Autoload\ClassLoader
     */
    protected $loader;

    /**
     * Loaded app configs
     * @var array
     */
    protected $apps;

    /**
     * Errors instance
     * @var Utils\Errors
     */
    protected $errors;

    /**
     * Logger instance
     * @var Utils\Logger
     */
    protected $logger;

    /**
     * Config instance
     *
     * @var Config\PhpConfig
     */
    protected $config;

    /**
     * Construct the things
     *
     * @param ClassLoader $loader Composer classloader
     */
    public function __construct(ClassLoader $loader, array $apps = []) {
        $this->loader = $loader;
        $this->apps = $apps;

        $this->config = new Config\PhpConfig();
        $this->config->loadFile('./config/global')
                ->loadFile('./config/local');

        $this->logger = new Utils\Logger($this->config);
        $this->errors = new Utils\Errors($this->config, $this->logger);
    }

    /**
     * Loads an app into a partial or full loaded state based
     * on provided app meta data.
     *
     * @param array $appData The app's meta data (version, path, name, etc.)
     * @param array $modules Modules to load
     *
     * @return \FW\Structures\App The booted app instance
     */
    public function load(string $appCode, array $modules = null) {
        if (!isset($this->apps[$appCode])) {
            throw new \InvalidArgumentException("App '{$app}' does not exist!");
        }

        $appMeta = $this->apps[$appCode];
        $appMeta['modules'] = $modules ?: [];

        return $this->loadApp($appMeta);
    }

    /**
     * Load an app
     * @param  array $appMeta  App metadata
     * @param  boolean $parentContainer   If the app is being booted as a dependent,
     *                                    this has access to the parent container.
     * @return \FW\Structures\App Application instance
     */
    public function loadApp(array $appMeta, Utils\Container $parentContainer = null) {
        $container = new Utils\Container(); // instantiate container

        $container->set('meta', $appMeta);
        $container->set('logger', $this->logger);
        $container->set('error_handler', $this->errors);

        $serverPath = "{$appMeta['path']}/server";
        $namespace = S::upperCamelize($appMeta['name']);
        $this->autoload($namespace, $serverPath);

        $configType = array_get($appMeta, 'configType', 'php');
        if (!isset(self::$default_configs[$configType])) {
            throw new \StatusException(
                "Invalid config type '{$configType}' specified for {$appMeta['name']}.",
                500,
                'SetupError'
            );
        }
        $configClass = self::$default_configs[$configType];
        $config = new $configClass(); // instantiate config

        // setup config in container
        $container->set('namespace', "\\{$namespace}\\");
        $container->set('module.config', $config);
        $container->set('events', function () {
            return new Utils\Events();
        });

        // load config with default modules
        $config->replace(Config\BaseConfig::getDefaultModules(), false);

        $config->replace($this->config->all(), true);
        $config->loadFile("$serverPath/config/app") // load the app-specific config file
            ->loadFile("$serverPath/config/local"); // load the app local file

        Utils\ContainerAccess::register($container, $namespace, empty($parentContainer));

        $main = $this->getMainClass($namespace);
        $app = new $main($container);

        // If it's in the full boot mode, set up the environment
        if (!$parentContainer) {
            $app->setupEnvironment(strtolower($config->get('mode', 'development')));
        }

        // load dependencies after basic app has been loaded
        // check for and load app dependencies on other apps (uses recursion!)
        $dependencies = array_get($appMeta, 'dependencies', []);
        // Iterate through app dependencies
        foreach (array_get($dependencies, 'apps', []) as $code => $version) {
            if (!isset($this->apps[$code])) {
                throw new \StatusException(
                    "Missing app dependency for '{$appMeta['name']}': '{$code}'",
                    500,
                    'SetupError'
                );
            }

            if (isset(self::$loadedApps[$code])) {
                $subApp = self::$loadedApps[$code];
            } else {
                $subApp = $this->loadApp($this->apps[$code], $container);
            }

            $container->set('app.' . $code, $subApp);
            $container->get('events')->on('post-init', [$subApp, 'loadAsDependent']);
        }

        $addons = System::discoverAddons("{$appMeta['path']}/addons", $config->get('dev-addons', false) ? './addons' : null);
        foreach (array_get($dependencies, 'addons', []) as $addon => $version) {
            if (!isset($addons[$addon])) {
                throw new \StatusException("Missing addon dependency for '{$appMeta['name']}: '{$addon}'");
            }

            $loadedAddon = $this->loadAddon($addon, $addons[$addon], $container);
            if ($loadedAddon) {
                $container->get('events')->on('post-init', [$loadedAddon, 'load']);
            }
        }

        $container->get('events')->trigger('pre-init', $container);

        $modules = array_get($appMeta, 'modules', []);
        $app->init($modules, empty($parentContainer));

        $container->get('events')->trigger('post-init', $container);

        return $app;
    }

    /**
     * Load an addon
     * @param  string $name      Addon name
     * @param  array  $addonData addon metadata
     * @param  \FW\Utils\Container $container Container instance
     */
    protected function loadAddon(string $name, array $addonData, Utils\Container $container) {
        $this->autoload($name, $addonData['path']);
        $namespace = S::upperCamelize($name);
        $addonClass = "\\{$namespace}\\Addon";

        if (!class_exists($addonClass) || !is_a($addonClass, '\FW\Structures\Addon', true)) {
            return;
        }

        $addon = new $addonClass($addonData);
        $container->set("addon.$name", $addon);
        return $addon;
    }

    /**
     * Parse config and return the app's main class
     *
     * @param string $namespace The app namespace
     * @return string The main class' classname
     */
    protected function getMainClass(string $namespace) {
        $mainClass = "\\{$namespace}\\App";

        // If main class doesn't exist, the app won't work, and thus dies
        if (!class_exists($mainClass) || !is_a($mainClass, '\FW\Structures\App', true)) {
            throw new \StatusException('No valid main class found for \'' . APPCODE . '\'.', 500, 'SetupError');
        }

        return $mainClass;
    }

    /**
     * Parse app meta data and return the config type (default is php)
     *
     * @param array $appData The app metadata
     * @return string The config class name
     * @throws \StatusException
     */
    protected function getConfigType(array $appData) {
        // if configType exists in the app metadata, use it, otherwise default to php
        $configName = array_key_exists('configType', $appData) ? $appData['configType'] : 'php';

        if (array_key_exists($configName, self::$default_configs)) {
            return self::$default_configs[$configName]; // if it is in the default config array, use it
        } else if (is_a($configName, '\FW\Config\BaseConfig', true)) {
            return $configName; // if it is an instance of BaseConfig, use it
        } else {
            // die because we need a config
            throw new \StatusException('Config implementation does not extend BaseConfig', 500, 'SetupError');
        }
    }

    /**
     * This adds an app or addon to the composer autoload function
     * The reason this exists is so that we don't have to override the
     * composer.json file every time we install an app (that would get rather tedious)
     *
     * @param  string $name App/Addon name (as loaded/parsed from the fw.json file)
     * @param  string $path path to the app/addon folder
     */
    protected function autoload(string $name, string $path) {
        $namespace = S::upperCamelize($name) . '\\';

        if (!array_key_exists($namespace, $this->loader->getPrefixesPsr4())) {
            $srcPath = $path . '/src/';
            if (file_exists($srcPath)) {
                $this->loader->addPsr4($namespace, $srcPath);
            }
        }

        if (file_exists($path . '/composer.json')) {
            if (!file_exists($path . '/vendor/autoload.php')) {
                throw new \StatusException("Missing composer dependencies for '{$name}'", 500, 'SetupError');
            }

            require_once($path . '/vendor/autoload.php');
        }
    }
}
