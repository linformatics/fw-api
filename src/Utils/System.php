<?php

namespace FW\Utils;

use Stringy\StaticStringy as S;

class System {
    /**
     * Cached apps for external use
     */
    public static $availableApps;

    /**
     * This function will recursively traverse the app folder looking for valid FW apps to choose from.
     * This replaces the former method of registering applications through an `apps.json` file
     * @param  string $appDir app directory
     * @return array          array of apps & app meta configs
     */
    public static function discoverApps(string $appDir) {
        $apps = self::find($appDir, function ($path, $appMeta) {
            if (!$appMeta || !isset($appMeta['appId'])) {
                return null;
            }

            // Add path to app meta
            $appMeta['path'] = $path;
            return [$appMeta['appId'] => $appMeta];
        });

        self::$availableApps = $apps;
        return $apps;
    }

    /**
     * Discovers addons from the app directory
     * @param  string $addonDir     Directory to search for addons
     * @param  string $globalPath   Global fallback if the app specific addon directory is missing. If unset, skips checking global addons
     * @return array associative array of addons name to addon metadata
     */
    public static function discoverAddons(string $addonDir, string $globalPath = null) {
        $loadAddon = function ($path, $addonMeta) {
            if (!$addonMeta || !isset($addonMeta['name'])) {
                return null;
            }

            $addonMeta['path'] = $path;
            return [S::dasherize($addonMeta['name'])->__toString() => $addonMeta];
        };

        $addons = self::find($addonDir, $loadAddon);
        // if a global path is given, load addons from there
        if ($globalPath) {
            // note global is first, meaning if the addon exists in the app then the app version will be used
            $addons = array_merge(self::find($globalPath, $loadAddon), $addons);
        }
        return $addons;
    }

    /**
     * Locates FW apps and addons in the specified directory
     * @param  string    $dir Directory to search for addons and apps
     * @param  \Closure  $fn  Closure to call when an addon or app is found.
     *                        The function is given two parameters: the path and the loaded meta.
     *                        Return an array to merge into the returned data array
     * @return array array of data merged from the closure
     */
    protected static function find(string $dir, \Closure $fn) {
        if (!file_exists($dir)) {
            return [];
        }

        // See the php docs on the FilesystemIterator for an explanation of what this is doing
        $files = new \FilesystemIterator($dir, \FilesystemIterator::KEY_AS_PATHNAME |
            \FilesystemIterator::CURRENT_AS_FILEINFO |
            \FilesystemIterator::FOLLOW_SYMLINKS |
            \FilesystemIterator::SKIP_DOTS);
        $meta = [];

        foreach ($files as $path => $file) {
            // Skip if not a directory
            if (!$file->isDir()) {
                continue;
            }

            // If fw.json doesn't exist, it's not a valid app or addon, so don't load it
            if (!file_exists("{$path}/fw.json")) {
                continue;
            }

            $appMeta = json_decode(file_get_contents("{$path}/fw.json"), true);

            if (file_exists("{$path}/config.json")) {
                $appMeta = $appMeta + json_decode(file_get_contents("{$path}/config.json"), true);
            }

            $result = $fn($path, $appMeta);

            if (!$result) {
                continue;
            }

            $meta = array_merge($meta, $result);
        }

        return $meta;
    }
}
