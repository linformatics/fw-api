<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Utils;

/**
 * Event handling for apps and addons
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 */
class Events {

    protected $events;

    /**
     * Construct the events utility
     */
    public function __construct() {
        $this->events = array();
    }

    /**
     * Triggers a particular event. Any arguments passed after the name
     * to trigger are passed as parameters to all of the functions that
     * are designed to be called when this event is fired.
     *
     * @param string $triggerName name to trigger
     * @param mixed $args,... arguments to pass to the functions
     */
    public function trigger() {
        $args = func_get_args();

        $triggerName = array_shift($args);

        if (isset($this->events[$triggerName])) {
            foreach ($this->events[$triggerName] as $callback) {
                call_user_func_array($callback, $args);
            }
        }
    }

    /**
     * Sets a function to be called when a certain trigger is fired
     * @param  string $triggerName trigger that the function is called on
     * @param  callable $callback  function to be called. Must return true for
     *                             is_callable
     */
    public function on($triggerName, $callback) {
        if (!is_callable($callback)) {
            throw new \InvalidArgumentException('Argument is not callable!');
        }

        if (!isset($this->events[$triggerName])) {
            $this->events[$triggerName] = array();
        }

        $this->events[$triggerName][] = $callback;
    }
}
