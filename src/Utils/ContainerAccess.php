<?php

namespace FW\Utils;

class ContainerAccess {

    protected static $containerInstances = [];
    public static $default;

    public static function register(Container $c, string $namespace, bool $default = true) {
        self::$containerInstances[$namespace] = $c;

        if ($default) {
            self::$default = $namespace;
        }
    }

    public static function deregister(string $namespace) {
        if (isset(self::$containerInstances[$namespace])) {
            unset(self::$containerInstances[$namespace]);
        }
    }

    public static function lookup(string $namespace) {
        $hasMatch = preg_match('/^\\\?(?P<ns>[A-Za-z]+)\\\/', $namespace, $matches) &&
            isset(self::$containerInstances[$matches['ns']]);

        return $hasMatch ? self::$containerInstances[$matches['ns']] : self::$containerInstances[self::$default];
    }
}
