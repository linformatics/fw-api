<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Utils;

/**
 * Standard Error handling module
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
class Errors {

    /**
     * Config module
     * @var \FW\Config\BaseConfig
     */
    protected $config;

    /**
     * Logger module
     * @var \FW\Logger\LoggerInterface
     */
    protected $logger;

    /**
     * Construct all the things!
     *
     * @param FWConfigBaseConfig $config
     * @param Logger                  $logger
     */
    public function __construct(\FW\Config\BaseConfig $config, Logger $logger) {
        $this->config = $config;
        $this->logger = $logger;

        set_error_handler(array($this, 'handleError'));
        set_exception_handler(array($this, 'handleException'));
        register_shutdown_function(array($this, 'handleFatal'));
    }

    /**
     * Handle exceptions
     * @param  \Exception $e exception to handle
     */
    public function handleException($e) {
        if (!is_a($e, '\Exception') && !is_a($e, '\Error')) {
            throw new \InvalidArgumentException(
                'This is an exception handler. You did not pass an exception. YOU SHALL NOT PASS!'
            );
        }

        $status = 200;
        $message = '';
        if ($e instanceof \StatusException) {
            $message = $e->getMessage();
            $status = $e->getCode();
        } else {
            $mode = $this->config->get('mode');
            if ($mode != 'prod' && $mode != 'production') {
                // ensure the client knows this is an error, as die skips the below code
                http_response_code(500);
                die($e->__toString());
            }
            $status = 500;
            $message = json_encode(array(
                'error' => true,
                'message' => 'An internal error occurred. Check your server logs for more details.',
                'type' => 'ServerError'
            ));
            $this->logger->writeLine($e->__toString());
        }
        //output exception
        http_response_code($status);
        header('Content-Type: application/json');
        echo $message;
        die;
    }

    /**
     * Handle PHP error
     * @param  int $errno
     * @param  string $errstr
     * @param  string $errfile
     * @param  int $errline
     * @throws \ErrorException
     */
    public function handleError($errno, $errstr, $errfile, $errline) {
        throw new \ErrorException($errstr, $errno, $errno, $errfile, $errline);
    }

    /**
     * Called on PHP shutdown to handle fatal errors
     */
    public function handleFatal() {
        $lastError = error_get_last();
        // this is run even if successful, so ensure its atually a fatal error
        if (!empty($lastError) && $lastError['type'] == E_ERROR) {
            header('Status: 500 Internal Server Error');
            header('HTTP/1.0 500 Internal Server Error');
        }
    }
}
