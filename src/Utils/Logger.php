<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Utils;

/**
 * Standard file logger
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
class Logger {

    /**
     * log file name
     * @var string
     */
    protected $fileName;

    /**
     * log directory
     * @var string
     */
    protected $logDir;

    /**
     * Construct the logger
     * @param FW\Config\BaseConfig $config Config implementation
     */
    public function __construct(\FW\Config\BaseConfig $config) {
        $this->logDir = $config->get('logs', getcwd() . '/logs/');
        $this->fileName = $this->logDir . date('Y-d-m') . '.log';
    }

    /** Ensures the directory for logs exists before writing to it */
    private function checkLogFolder() {
        if (!is_dir($this->logDir)) {
            mkdir($this->logDir);
        }
    }

    /**
     * Writes a line to the file log
     * @param  string $message message to write
     */
    public function writeLine($message) {
        $this->checkLogFolder();
        error_log($message, 3, $this->fileName);
    }

    /**
     * Clears a file log
     */
    public function clear() {
        $this->checkLogFolder();
        file_put_contents($this->fileName, '');
    }
}
