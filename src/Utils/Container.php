<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Utils;

/**
 * Container class for holding module objects
 *
 * Uses the pimple container library (http://pimple.sensiolabs.org)
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
class Container extends \Pimple\Container {

    /**
     * Gets a container value
     * @param  string $id id to get
     * @return mixed      object gotten
     */
    public function get($id) {
        $explodedKey = explode('.', $id);

        if ($explodedKey[0] == 'this') {
            $meta = $this->offsetGet('meta');
            return count($explodedKey) > 1 ? $meta[$explodedKey[1]] ?? null : null;
        }

        return $this->offsetGet($id);
    }

    /**
     * Sets a container value
     * @param string $id    id to set
     * @param mixed $value value to set
     */
    public function set($id, $value) {
        return $this->offsetSet($id, $value);
    }

    /**
     * Checks if the container has a certain value
     * @param  string  $id id to check
     * @return boolean     if the id exists in the container
     */
    public function has($id) {
        return $this->offsetExists($id);
    }

    /**
     * Removes a value from the container
     * @param  string $id id to remove
     * @return mixed     removed object
     */
    public function remove($id) {
        return $this->offsetUnset($id);
    }
}
