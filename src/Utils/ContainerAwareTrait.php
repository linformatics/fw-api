<?php

namespace FW\Utils;

trait ContainerAwareTrait {

    final protected static function inject($key) {
        $container = ContainerAccess::lookup(get_called_class());
        return $container->get($key);
    }
}
