<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Config;

/**
 * Json implmentation of the config module
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
class JsonConfig extends BaseConfig {

    /**
     * Default file extension to append to the filename
     * @var string
     */
    const DEFAULT_EXTENSION = '.json';

    /**
     * {@inheritdoc}
     *
     * @param string $fileName The name of the file
     * @param boolean $merge Whether or not to merge the data
     * @return \FW\Config\JsonConfig The config instance
     */
    public function loadFile($fileName, $merge = true) {
        $fileName = $fileName . self::DEFAULT_EXTENSION;
        if (file_exists($fileName)) {
            $array = json_decode(file_get_contents($fileName), true);
            $this->replace($array, $merge);
        }

        return $this;
    }
}
