<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Config;

/**
 * Abstract BaseConfig implementation
 * Makes it simple to declare a new Config interface implementation
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since 1.0.0
 */
abstract class BaseConfig {

    /**
     * Default modules implementation array
     *
     * @var array
     */
    private static $default_modules = array(
        'module.router'=>'\FW\Router\Standard\SlimRouter',
        'module.adapter'=>'\FW\Data\Standard\FWAdapter',
        'module.view'=>'\FW\View\Standard\FWViewManager'
    );

    // TODO: Add support for aliasing of keys

    /**
     * Array of config values
     * @var array
     */
    protected $values;

    /**
     * Constructs an empty config
     */
    public function __construct() {
        $this->values = array();
    }

    /**
     * {@inheritdoc}
     * @param  string $id key to get
     * @param  value to return if id does not exist (default null)
     * @return mixed value if located, otherwise $alt
     */
    public function get($id, $alt = null) {
        return array_get($this->values, $id, $alt);
    }

    /**
     * {@inheritdoc}
     * @param string $id key to set
     * @param value $value mixed value to set
     * @param boolean $merge Whether or not to merge (if value is an array)
     * @param boolean $assoc Whether or not the merged array is associative
     * @return \FW\Config\PhpConfig
     */
    public function set($id, $value, $merge = true, $assoc = false) {
        if ($merge && is_array($value) && $this->has($id) && is_array($this->values[$id])) {
            $merged = array_replace_recursive($this->values[$id], $value);
            $value = $assoc ? $this->removeNumIndexes($merged) : $merged;
        }

        array_set($this->values, $id, $value);
        return $this;
    }

    /**
     * {@inheritdoc}
     * @param  array   $arr   array to merge or replace
     * @param  boolean $merge whether or not to merge
     * @return \FW\Config\PhpConfig
     */
    public function replace(array $arr, $merge = true) {
        if ($merge) {
            $this->values = $this->removeNumIndexes(array_replace_recursive($this->values, $arr));
        } else {
            $this->values = $arr;
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     * @param  string  $id key to look up
     * @return boolean     whether or not the key exists
     */
    public function has($id) {
        return array_has($this->values, $id);
    }

    /**
     * {@inheritdoc}
     * @param  string $id key to remove
     * @return mixed      object removed
     */
    public function remove($id) {
        if (!$id) {
            $this->values = array();
            return null;
        } else if (!$this->has($id)) {
            return null;
        }

        $obj = array_get($this->values, $id);
        array_remove($this->values, $id);
        return $obj;
    }

    /**
     * Gets all the values from the array
     * @return array all the values
     */
    public function all() {
        return $this->values;
    }


    /**
     * Tells whether or not a given module implementation is the
     * default implementation
     *
     * @param string $module Module name
     * @param string $class Class name to check
     * @return boolean whether or not it is the default
     */
    public function isDefaultModule($module) {
        $moduleName = "module.{$module}";

        return isset(self::$default_modules[$moduleName])
            && isset($this->values[$moduleName])
            && self::$default_modules[$moduleName] == $this->values[$moduleName];
    }

    /**
     * Helper function
     * @param  array $array  array to strip numeric indexes from
     * @return array array less numeric indexes
     */
    protected function removeNumIndexes($array) {
        foreach (array_keys($array) as $key) {
            if (is_numeric($key)) {
                unset($array[$key]);
            }
        }
        return $array;
    }

    /**
     * {@inheritdoc}
     *
     * @param string $fileName The name of the file
     * @param boolean $merge Whether or not to merge the data from the file with existing config data
     */
    abstract public function loadFile($fileName, $merge = true);

    /**
     * Gets the default list of modules
     *
     * @return array assoc. array of default modules indexed by module name
     */
    public static function getDefaultModules() {
        return self::$default_modules;
    }
}
