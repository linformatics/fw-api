<?php
/**
 * FW App System
 *
 * @copyright 2015-2016 Benner Informatics
 * @version   1.0.0
 */

namespace FW\Config;

/**
 * Php Array implmentation of the config module
 *
 * @author Austin Burdine <aburdine@olivet.edu>
 * @since  1.0.0
 */
class PhpConfig extends BaseConfig {

    /**
     * File extension to append to the filename in loadFile
     * @var string
     */
    const DEFAULT_EXTENSION = '.php';

    /**
     * {@inheritdoc}
     *
     * @param string $fileName The name of the file
     * @param boolean $merge Whether or not to merge the data
     * @return \FW\Config\PhpConfig The config instance
     */
    public function loadFile($fileName, $merge = true) {
        $fileName = $fileName . self::DEFAULT_EXTENSION;
        if (file_exists($fileName)) {
            $array = (array) require($fileName);
            $this->replace($array, $merge);
        }

        return $this;
    }
}
